import { describe, expect, test } from "@jest/globals";
import { Day } from "../globals";

type Coordinates = {
  row: number;
  col: number;
};

enum VisibilityDirection {
  FROM_ABOVE = "above",
  FROM_RIGHT = "right",
  FROM_BELOW = "below",
  FROM_LEFT = "left",
}

class Tree {
  private heights: Map<VisibilityDirection, number> = new Map();

  constructor(
    private grid: Grid,
    public coordinates: Coordinates,
    private height: number
  ) {}

  public viewingDistance(direction: VisibilityDirection, from?: Tree) {
    // just for convenience, in the first call to this function, `from` will just be this tree
    if (from === undefined) {
      from = this;
    }

    const neighbor = this.grid.neighbor(this, direction);
    if (neighbor === undefined) {
      // there are no trees left in that direction, stop counting
      return 0;
    }

    if (neighbor.higherThanOrEqual(from)) {
      // if the next tree in that direction is higher than where we're looking from, that means
      // we are the last visible tree. stop counting, but do count ourselves as the last visible tree
      return 1;
    }

    // otherwise let the neighbor count the trees in that direction that are visible from where we're looking from
    return neighbor.viewingDistance(direction, from) + 1;
  }

  public getScenicScore() {
    return Object.values(VisibilityDirection).reduce(
      (accumulator, currentValue) =>
        accumulator * this.viewingDistance(currentValue),
      1
    );
  }

  public getNeighborPerspective(direction: VisibilityDirection) {
    if (this.heights.has(direction)) {
      return this.heights.get(direction);
    }

    const neighbor = this.grid.neighbor(this, direction);
    if (neighbor === undefined) {
      // we don't have a neighbor in this direction, set to lowest height possible
      return -Infinity;
    }

    const highestFromNeighborPerspective =
      neighbor.highestInDirection(direction);
    this.heights.set(direction, highestFromNeighborPerspective);
    return highestFromNeighborPerspective;
  }

  private higherThanOrEqual(tree: Tree): boolean {
    return this.height >= tree.height;
  }

  public highestInDirection(direction: VisibilityDirection) {
    const highestFromNeighborPerspective =
      this.getNeighborPerspective(direction);

    const highestFromOurPerspective =
      this.height > highestFromNeighborPerspective
        ? this.height
        : highestFromNeighborPerspective;
    return highestFromOurPerspective;
  }

  public visibleFrom(direction: VisibilityDirection): boolean {
    return this.height > this.getNeighborPerspective(direction);
  }

  public isVisibleAtAll(): boolean {
    return Object.values(VisibilityDirection).some((direction) =>
      this.visibleFrom(direction)
    );
  }

  public isOuter(): boolean {
    // we are on the outer edge if at least in one direction there isn't a neighboring tree
    return Object.values(VisibilityDirection).some(
      (direction) => this.grid.neighbor(this, direction) === undefined
    );
  }
}

class Grid extends Map<`${Coordinates["row"]}|${Coordinates["col"]}`, Tree> {
  public static fromInput(input: string[]): Grid {
    const grid = new Grid();

    for (const [row, string] of input.entries()) {
      for (const [col, height] of string.split("").entries()) {
        grid.insertTree({ row, col }, Number(height));
      }
    }

    return grid;
  }

  public getHighestScenicScore(): number {
    return Array.from(this.values()).reduce((accumulator, tree) => {
      const scenicScore = tree.getScenicScore();
      if (scenicScore > accumulator) {
        return scenicScore;
      }
      return accumulator;
    }, 0);
  }

  public countVisibleTrees(): number {
    return Array.from(this.values()).filter((tree) => tree.isVisibleAtAll())
      .length;
  }

  public countVisibleInteriorTrees(): number {
    return Array.from(this.values()).filter(
      (tree) => !tree.isOuter() && tree.isVisibleAtAll()
    ).length;
  }

  public countVisibleEdgeTrees(): number {
    // edge trees are always visible, so just count the amount of outer edge trees in general
    return Array.from(this.values()).filter((tree) => tree.isOuter()).length;
  }

  public getFromCoordinates(coordinates: Coordinates) {
    return this.get(`${coordinates.row}|${coordinates.col}`);
  }

  private setFromCoordinates(coordinates: Coordinates, data: Tree) {
    this.set(`${coordinates.row}|${coordinates.col}`, data);
  }

  public insertTree(coordinates: Coordinates, height: number) {
    this.setFromCoordinates(coordinates, new Tree(this, coordinates, height));
  }

  public neighbor(tree: Tree, direction: VisibilityDirection): Tree {
    switch (direction) {
      case VisibilityDirection.FROM_ABOVE:
        return this.getFromCoordinates({
          row: tree.coordinates.row - 1,
          col: tree.coordinates.col,
        });
      case VisibilityDirection.FROM_RIGHT:
        return this.getFromCoordinates({
          row: tree.coordinates.row,
          col: tree.coordinates.col + 1,
        });
      case VisibilityDirection.FROM_BELOW:
        return this.getFromCoordinates({
          row: tree.coordinates.row + 1,
          col: tree.coordinates.col,
        });
      case VisibilityDirection.FROM_LEFT:
        return this.getFromCoordinates({
          row: tree.coordinates.row,
          col: tree.coordinates.col - 1,
        });
    }
  }
}

describe("Day 8", () => {
  const day = new Day(__dirname, {
    split: true,
  });

  describe("Part 1", () => {
    test("Example", async () => {
      const { example } = await day.getInputs();

      const grid = Grid.fromInput(example);

      const topLeft5 = grid.getFromCoordinates({ row: 1, col: 1 });
      expect(topLeft5.visibleFrom(VisibilityDirection.FROM_LEFT)).toBe(true);
      expect(topLeft5.visibleFrom(VisibilityDirection.FROM_ABOVE)).toBe(true);
      expect(topLeft5.visibleFrom(VisibilityDirection.FROM_RIGHT)).toBe(false);
      expect(topLeft5.visibleFrom(VisibilityDirection.FROM_BELOW)).toBe(false);

      const topMiddle5 = grid.getFromCoordinates({ row: 1, col: 2 });
      expect(topMiddle5.visibleFrom(VisibilityDirection.FROM_ABOVE)).toBe(true);
      expect(topMiddle5.visibleFrom(VisibilityDirection.FROM_RIGHT)).toBe(true);

      const topRight1 = grid.getFromCoordinates({ row: 1, col: 3 });
      expect(topRight1.isVisibleAtAll()).toBe(false);

      const leftMiddle5 = grid.getFromCoordinates({ row: 2, col: 1 });
      expect(leftMiddle5.visibleFrom(VisibilityDirection.FROM_RIGHT)).toBe(
        true
      );

      const center3 = grid.getFromCoordinates({ row: 2, col: 2 });
      expect(center3.isVisibleAtAll()).toBe(false);

      const rightMiddle3 = grid.getFromCoordinates({ row: 2, col: 3 });
      expect(rightMiddle3.visibleFrom(VisibilityDirection.FROM_RIGHT)).toBe(
        true
      );

      const bottomLeft3 = grid.getFromCoordinates({ row: 3, col: 1 });
      expect(bottomLeft3.isVisibleAtAll()).toBe(false);
      const bottomMiddle5 = grid.getFromCoordinates({ row: 3, col: 2 });
      expect(bottomMiddle5.isVisibleAtAll()).toBe(true);
      const bottomRight4 = grid.getFromCoordinates({ row: 3, col: 3 });
      expect(bottomRight4.isVisibleAtAll()).toBe(false);

      expect(grid.countVisibleEdgeTrees()).toBe(16);
      expect(grid.countVisibleInteriorTrees()).toBe(5);
      expect(grid.countVisibleTrees()).toEqual(21);
    });

    test("Input", async () => {
      const { input } = await day.getInputs();

      const grid = Grid.fromInput(input);
      expect(grid.countVisibleTrees()).toEqual(1787);
    });
  });

  describe("Part 2", () => {
    test("Example", async () => {
      const { example } = await day.getInputs();
      const grid = Grid.fromInput(example);

      const middle5 = grid.getFromCoordinates({ row: 1, col: 2 });
      expect(middle5.viewingDistance(VisibilityDirection.FROM_ABOVE)).toBe(1);
      expect(middle5.viewingDistance(VisibilityDirection.FROM_LEFT)).toBe(1);
      expect(middle5.viewingDistance(VisibilityDirection.FROM_RIGHT)).toBe(2);
      expect(middle5.viewingDistance(VisibilityDirection.FROM_BELOW)).toBe(2);
      expect(middle5.getScenicScore()).toBe(4);

      const middle5FourthRow = grid.getFromCoordinates({ row: 3, col: 2 });
      expect(
        middle5FourthRow.viewingDistance(VisibilityDirection.FROM_ABOVE)
      ).toBe(2);
      expect(
        middle5FourthRow.viewingDistance(VisibilityDirection.FROM_LEFT)
      ).toBe(2);
      expect(
        middle5FourthRow.viewingDistance(VisibilityDirection.FROM_BELOW)
      ).toBe(1);
      expect(
        middle5FourthRow.viewingDistance(VisibilityDirection.FROM_RIGHT)
      ).toBe(2);
      expect(middle5FourthRow.getScenicScore()).toBe(8);

      expect(grid.getHighestScenicScore()).toBe(8);
    });

    test("Input", async () => {
      const { input } = await day.getInputs();
      const grid = Grid.fromInput(input);

      expect(grid.getHighestScenicScore()).toBe(440640);
    });
  });
});
