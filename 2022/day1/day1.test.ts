import { describe, expect, test } from "@jest/globals";
import { Day, sumArray } from "../globals";

function* parseInput(input: string[]) {
  let numbers: number[] = [];
  for (const line of input) {
    if (line !== "") {
      numbers.push(Number.parseInt(line));
    } else {
      yield numbers;
      numbers = [];
    }
  }
  if (numbers.length !== 0) {
    yield numbers;
  }
}

const part1 = (input: string[]) => {
  let maxCalories = 0;

  for (const caloriesList of parseInput(input)) {
    const caloriesCarried = sumArray(caloriesList);

    if (caloriesCarried > maxCalories) {
      maxCalories = caloriesCarried;
    }
  }

  return maxCalories;
};

const part2 = (input: string[]) => {
  const maxCaloriesToKeep = 3;
  let maxCalories: number[] = new Array(maxCaloriesToKeep).fill(0);

  for (const caloriesList of parseInput(input)) {
    const caloriesCarried = sumArray(caloriesList);

    const lowestTopCalory = maxCalories.pop(); // maxCalories is sorted desc, so last element is lowest calory
    if (caloriesCarried > lowestTopCalory) {
      maxCalories.push(caloriesCarried);
    }
    if (maxCalories.length < maxCaloriesToKeep) {
      maxCalories.push(lowestTopCalory);
    }
    // need to provide sort fn, default sorting converts all elements to strings then compares UTF code units oh god
    maxCalories.sort((a, b) => b - a);
  }

  return sumArray(maxCalories);
};

describe("Day 1", () => {
  const day = new Day(__dirname, {
    split: true,
  });

  test("Part 1", async () => {
    const { example, input } = await day.getInputs();
    expect(part1(example)).toEqual(24000);
    expect(part1(input)).toEqual(71471);
  });

  test("Part 2", async () => {
    const { example, input } = await day.getInputs();
    expect(part2(example)).toEqual(45000);
    expect(part2(input)).toEqual(211189);
  });
});
