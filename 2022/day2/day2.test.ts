import { describe, expect, test } from "@jest/globals";
import { Day, sumArray } from "../globals";

enum Shape {
  ROCK = "rock",
  PAPER = "paper",
  SCISSORS = "scissors",
}

enum Outcome {
  WIN = "win",
  TIE = "tie",
  LOSE = "lose",
}

const mapInputToShape: Record<string, Shape> = {
  A: Shape.ROCK,
  B: Shape.PAPER,
  C: Shape.SCISSORS,
  X: Shape.ROCK,
  Y: Shape.PAPER,
  Z: Shape.SCISSORS,
};

const mapOutcomeToScore: Record<Outcome, 6 | 3 | 0> = {
  [Outcome.WIN]: 6,
  [Outcome.TIE]: 3,
  [Outcome.LOSE]: 0,
};

const mapShapeToScore: Record<Shape, 1 | 2 | 3> = {
  [Shape.ROCK]: 1,
  [Shape.PAPER]: 2,
  [Shape.SCISSORS]: 3,
};

const part1 = (input: string[]) => {
  type Round = {
    opponent: Shape;
    you: Shape;
  };

  const mapRoundToOutcome = ({ opponent, you }: Round): Outcome => {
    switch (true) {
      case opponent === Shape.SCISSORS && you === Shape.ROCK:
      case opponent === Shape.PAPER && you === Shape.SCISSORS:
      case opponent === Shape.ROCK && you === Shape.PAPER:
        return Outcome.WIN;
      case opponent === you:
        return Outcome.TIE;
      default:
        return Outcome.LOSE;
    }
  };

  const scores = input.map((roundString) => {
    const [opponent, you] = roundString
      .split(" ")
      .map((input) => mapInputToShape[input]);
    const round: Round = {
      opponent,
      you,
    };

    const scoreFromShape = mapShapeToScore[you];
    const scoreFromOutcome = mapOutcomeToScore[mapRoundToOutcome(round)];

    return scoreFromShape + scoreFromOutcome;
  });

  return sumArray(scores);
};

const part2 = (input: string[]) => {
  const mapInputToOutcome: Record<string, Outcome> = {
    X: Outcome.LOSE,
    Y: Outcome.TIE,
    Z: Outcome.WIN,
  };

  const mapShapeToOutcome: Record<Shape, Record<Outcome, Shape>> = {
    [Shape.ROCK]: {
      [Outcome.WIN]: Shape.PAPER,
      [Outcome.TIE]: Shape.ROCK,
      [Outcome.LOSE]: Shape.SCISSORS,
    },
    [Shape.PAPER]: {
      [Outcome.WIN]: Shape.SCISSORS,
      [Outcome.TIE]: Shape.PAPER,
      [Outcome.LOSE]: Shape.ROCK,
    },
    [Shape.SCISSORS]: {
      [Outcome.WIN]: Shape.ROCK,
      [Outcome.TIE]: Shape.SCISSORS,
      [Outcome.LOSE]: Shape.PAPER,
    },
  };

  const scores = input.map((roundString) => {
    const [opponentString, suggestionString] = roundString.split(" ");
    const opponent = mapInputToShape[opponentString];
    const suggestion = mapInputToOutcome[suggestionString];

    const you = mapShapeToOutcome[opponent][suggestion];

    const scoreFromShape = mapShapeToScore[you];
    const scoreFromOutcome = mapOutcomeToScore[suggestion];

    return scoreFromShape + scoreFromOutcome;
  });
  return sumArray(scores);
};

describe("Day 2", () => {
  const day = new Day(__dirname, {
    split: true,
  });

  test("Part 1", async () => {
    const { example, input } = await day.getInputs();
    expect(part1(example)).toEqual(15);
    expect(part1(input)).toEqual(17189);
  });

  test("Part 2", async () => {
    const { example, input } = await day.getInputs();
    expect(part2(example)).toEqual(12);
    expect(part2(input)).toEqual(13490);
  });
});
