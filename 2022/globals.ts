import { readFile } from "node:fs/promises";
import { join } from "path";
import { EOL } from "os";

type Options = {
  split: boolean;
};

type ReturnsWholeOrLines<O extends Options> = O["split"] extends true
  ? string[]
  : string;

export class Day<O extends Options> {
  constructor(private dirname: string, private options: O) {}

  private async readFile(filename: string) {
    return (await readFile(join(this.dirname, filename), "utf-8")).trimEnd(); // trim end to remove last \n
  }

  public async getFileContents(
    filename: string
  ): Promise<ReturnsWholeOrLines<O>>;
  public async getFileContents(filename: string) {
    const contents = await this.readFile(filename);

    if (this.options.split) {
      return contents.split(EOL);
    }

    return contents;
  }

  public getExample = async () => await this.getFileContents("example");
  public getInput = async () => await this.getFileContents("input");
  public getInputs = async () => ({
    example: await this.getExample(),
    input: await this.getInput(),
  });
}

export const sumArray = (scores: number[]) =>
  scores.reduce((accumulator, current) => accumulator + current, 0);
