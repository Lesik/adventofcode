import { describe, expect, test } from "@jest/globals";
import { Day } from "../globals";

const detectStart = (input: string, sequenceLength: number) => {
  for (let i = 0; i < input.length; i++) {
    const characters = new Set(input.slice(i, i + sequenceLength));
    const charactersAreUnique = characters.size === sequenceLength;

    if (charactersAreUnique) {
      return i + sequenceLength;
    }
  }

  return -1;
};

const part1 = (input: string) => detectStart(input, 4);
const part2 = (input: string) => detectStart(input, 14);

describe("Day 6", () => {
  const day = new Day(__dirname, {
    split: false,
  });

  test("Part 1", async () => {
    expect(part1("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).toEqual(7);
    expect(part1("bvwbjplbgvbhsrlpgdmjqwftvncz")).toEqual(5);
    expect(part1("nppdvjthqldpwncqszvftbrmjlhg")).toEqual(6);
    expect(part1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).toEqual(10);
    expect(part1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).toEqual(11);

    const input = await day.getInput();
    expect(part1(input)).toEqual(1538);
  });

  test("Part 2", async () => {
    expect(part2("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).toEqual(19);
    expect(part2("bvwbjplbgvbhsrlpgdmjqwftvncz")).toEqual(23);
    expect(part2("nppdvjthqldpwncqszvftbrmjlhg")).toEqual(23);
    expect(part2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).toEqual(29);
    expect(part2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).toEqual(26);

    const input = await day.getInput();
    expect(part2(input)).toEqual(2315);
  });
});
