import { describe, expect, test } from "@jest/globals";
import { Day, sumArray } from "../globals";

const lowercase = [...Array(26)].map((_, index) => [
  String.fromCharCode(index + 97),
  index + 1,
]);

const uppercase = [...Array(26)].map((_, index) => [
  String.fromCharCode(index + 65),
  index + 27,
]);

const mapLetterToPriority = Object.fromEntries([...lowercase, ...uppercase]);

const part1 = (input: string[]) => {
  const priorities = input.map((rucksack) => {
    const compartment1 = rucksack.substring(0, rucksack.length / 2);
    const compartment2 = rucksack.substring(rucksack.length / 2);

    const duplicateLetter = compartment1
      .split("")
      .find((letter) => compartment2.includes(letter));

    return mapLetterToPriority[duplicateLetter];
  });

  return sumArray(priorities);
};

function* chunkArray<T>(array: T[], chunkSize: number) {
  for (let i = 0; i < array.length; i += chunkSize) {
    yield array.slice(i, i + chunkSize);
  }
}

const part2 = (input: string[]) => {
  let prioritiesSum = 0;

  for (const group of chunkArray(input, 3)) {
    const [firstMember, ...otherGroupMembers] = group;

    const groupBadge = firstMember
      .split("")
      .find((letter) =>
        otherGroupMembers.every((otherGroupMember) =>
          otherGroupMember.includes(letter)
        )
      );

    const priority = mapLetterToPriority[groupBadge];
    prioritiesSum += priority;
  }

  return prioritiesSum;
};

describe("Day 3", () => {
  const day = new Day(__dirname, {
    split: true,
  });

  test("Part 1", async () => {
    const { example, input } = await day.getInputs();
    expect(part1(example)).toEqual(157);
    expect(part1(input)).toEqual(7967);
  });

  test("Part 2", async () => {
    const { example, input } = await day.getInputs();
    expect(part2(example)).toEqual(70);
    expect(part2(input)).toEqual(2716);
  });
});
